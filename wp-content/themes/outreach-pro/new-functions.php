<?php
//Function for Home Top.

function home_top_slider() {
	?>
	<div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="slider_area">
            <!-- Start super slider -->
            <div id="slides">
              <ul class="slides-container">                          
                
<?php
  global $post;
  $args = array(
    'showposts' => -1,
    'post_type' => 'slides',
    'post_status' => 'publish',
    'order' => 'ASC',

  );
  query_posts( $args );


    if (have_posts()) : while (have_posts()) : the_post(); 

?>
                <li>
                  <!-- <img src="http://localhost/wpf-degree/wp-content/uploads/2015/12/2.jpg" alt="img"> -->
                  <?php echo get_the_post_thumbnail($post->ID); ?>
                   <div class="slider_caption">
                    <h2><?php echo get_the_title(); ?></h2>
                    <p><?php echo get_the_content(); ?></p>
                    <a class="slider_btn" href="<?php echo get_the_excerpt(); ?>">Know More</a>
                  </div>
                </li>
<?php
  endwhile; 
  endif;
  wp_reset_query();
?>
              </ul>
              <nav class="slides-navigation">
                <a href="#" class="next"></a>
                <a href="#" class="prev"></a>
              </nav>
            </div>
          </div>
        </div>
      </div>
	<?php
}
add_shortcode( 'home-top-slider', 'home_top_slider' );


//Function for Home Bottom.




//Function for midsection-one
function foobar_func( $atts ){
?>
<div class="row">        
        <div class="col-lg-12 col-sm-12">
          <div class="whyus_top">
            <div class="container">
              <!-- Why us top titile -->
              <div class="row">
                <div class="col-lg-12 col-md-12"> 
                  <div class="title_area">
                    <h2 class="title_two">Why Us</h2>
                    <span></span> 
                  </div>
                </div>
              </div>
              <!-- End Why us top titile -->
              <!-- Start Why us top content  -->
              <div class="row">
                
              <?php
              global $post;
              $args = array(
			    'showposts' => 4,
			    'post_type' => 'page',
			    'post_status' => 'publish',
			    'order' => 'ASC',
			    'meta_query' => array(
	        array(
	           'key' => 'custom-image',
	       )
		)
				);
				query_posts( $args );


               if (have_posts()) : while (have_posts()) : the_post(); 
              ?>

                <div class="col-lg-3 col-md-3 col-sm-3">
                  <div class="single_whyus_top wow fadeInUp">
                    <div class="whyus_icon">
                    	<?php $custom_img_class = get_post_meta( $post->ID, 'custom-image' , true); 
                    	 ?>
                      <span class="fa <?php echo $custom_img_class ?>"></span>
                    </div>
                    <h3><?php echo get_the_title(); ?></h3>
                    <p><?php echo get_the_content(); ?></p>
                  </div>
                </div>
                
                
                
                <?php 
                
                endwhile;
                endif;
                wp_reset_query();
                ?>


              </div>
              <!-- End Why us top content  -->
            </div>
          </div>
        </div>        
</div>

<?php
}
add_shortcode( 'why-us', 'foobar_func' );


// Function for courses section


function courses_section() {
	?>
	<div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Our Courses</h2>
              <span></span> 
            </div>
          </div>
        </div>
        <!-- End Our courses titile -->
        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="ourCourse_content">
              <ul class="course_nav">
                
              <?php
              global $post;
              $args = array(
                'showposts' => -1,
                'post_type' => 'courses',
                'post_status' => 'publish',
                'order' => 'DESC',
              );
              query_posts( $args );


              if (have_posts()) : while (have_posts()) : the_post(); 
              ?>


                <li>
                  <div class="single_course">
                    <div class="singCourse_imgarea">
                      <?php echo get_the_post_thumbnail($post->ID); ?>
                      <div class="mask">                         
                        <a href="<?php echo get_the_permalink(); ?>" class="course_more">View Course</a>
                      </div>
                    </div>
                    <div class="singCourse_content">
                    <h3 class="singCourse_title"><a href="#"><?php echo get_the_title(); ?></a></h3>
                    <?php $course_cost = get_post_meta($post->ID, 'wpcf-course-cost', true); ?>
                    <?php $course_cost_remarks = get_post_meta($post->ID, 'wpcf-course-cost-remarks', true); ?>
                    <p class="singCourse_price"><span><?php echo $course_cost; ?></span>&nbsp;<?php echo $course_cost_remarks; ?> </p>
                    <p><?php echo get_the_content(); ?></p>
                    </div>
                    <div class="singCourse_author">
                      <?php $lecturer_image = get_post_meta($post->ID, 'wpcf-lecturer-image', true); ?>
                      <img src="<?php echo $lecturer_image; ?>" alt="img">
                      <?php $lecturer = get_post_meta($post->ID, 'wpcf-lecturer', true); ?>
                      <p><?php echo $lecturer; ?></p>
                    </div>
                  </div>
                </li>

                  <?php
                  endwhile;
                  endif;
                  wp_reset_query();
                  ?>            
              
              </ul>
            </div>
          </div>
        </div>
	<?php
}

add_shortcode( 'wpf-courses', 'courses_section' );

//Function for tutors section.

function tutors_section() {
	?>
	<div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Our Tutors</h2>
              <span></span> 
            </div>
          </div>
        </div>
        <!-- End Our courses titile -->

        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="ourTutors_content">
              <!-- Start Tutors nav -->
              <ul class="tutors_nav">
              <?php
              global $post;
              $args = array(
			    'showposts' => -1,
			    'post_type' => 'tutors',
			    'post_status' => 'publish',
			    'order' => 'DESC',
				);
				query_posts( $args );

				if (have_posts()) : while (have_posts()) : the_post(); 
              ?>
                <li>
                  <div class="single_tutors">
                    <div class="tutors_thumb">
                      <?php echo get_the_post_thumbnail(); ?>                     
                    </div>
                    <div class="singTutors_content">
                      <h3 class="tutors_name"><?php echo get_the_title(); ?></h3>
                      <?php  $lecturer = get_post_meta($post->ID, 'wpcf-designation', true); ?>
                      <span>Technology Teacher</span>
                      <p><?php echo get_the_content(); ?></p>
                    </div>
                    <div class="singTutors_social">
                      <ul class="tutors_socnav">
                      <?php
                      	$facebook_link = get_post_meta($post->ID, 'wpcf-facebook-url', true);
                      	$twitter_link = get_post_meta($post->ID, 'wpcf-twitter-link', true);
                      	$insta_link = get_post_meta($post->ID, 'wpcf-instagram-url', true);
                      	$gplus_link = get_post_meta($post->ID, 'wpcf-google-plus-link', true);
                      ?>
                        <?php if($facebook_link) { ?><li><a class="fa fa-facebook" href="<?php echo $facebook_link; ?>"></a></li><?php }?>
                        <?php if($twitter_link) { ?><li><a class="fa fa-twitter" href="<?php echo $twitter_link; ?>"></a></li><?php }?>
                        <?php if($insta_link) { ?><li><a class="fa fa-instagram" href="<?php echo $insta_link; ?>"></a></li><?php }?>
                        <?php if($gplus_link) { ?><li><a class="fa fa-google-plus" href="<?php echo $gplus_link; ?>"></a></li><?php }?>
                      </ul>
                    </div>
                  </div>
                </li> 
                 <?php
                 endwhile;
                 endif;
                 wp_reset_query();
                 ?>                                     
              </ul>
            </div>
          </div>
        </div>
	<?php
}
add_shortcode( 'wpf-tutors', 'tutors_section' );


// Function for testimonials section

function home_testimonials() {
	?>
	<div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">What our Student says</h2>
              <span></span> 
            </div>
          </div>
        </div>
        <!-- End Our courses titile -->

        <!-- Start Our courses content -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="studentsTestimonial_content">              
              <div class="row">
              <?php
              global $post;
              $args = array(
			    'showposts' => 3,
			    'post_type' => 'testimonial',
			    'post_status' => 'publish',
			    'order' => 'DESC',
				);
				query_posts( $args );

				if (have_posts()) : while (have_posts()) : the_post(); 
              ?>

                <!-- start single student testimonial -->
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <div class="single_stsTestimonial wow fadeInUp">
                    <div class="stsTestimonial_msgbox">
                      <p><?php echo get_the_content(); ?></p>
                    </div>
                    <img class="stsTesti_img" src="http://localhost/wpf-degree/wp-content/uploads/2015/12/author.jpg" alt="img">
                    <div class="stsTestimonial_content">
                      <h3><?php echo get_the_title(); ?></h3>                      
                      <span>Ex. Student</span>
                      <p>Software Department</p>
                    </div>
                  </div>
                </div>
                <!-- End single student testimonial -->
                <?php
                endwhile;
                endif;
                wp_reset_query();
                ?>
              </div>
            </div>
          </div>
        </div>
	<?php
}
add_shortcode( 'home-testimonials', 'home_testimonials' );

