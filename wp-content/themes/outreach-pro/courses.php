<?php
/**
 * This file adds the Home Page to the Outreach Pro Theme.
 *
 * @author StudioPress
 * @package Outreach Pro
 * @subpackage Customizations
 * 
 * 
 * Template Name: courses
 */
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

add_action( 'genesis_after_header', 'outreach_inner_section_bottom_widgets' );
function outreach_inner_section_bottom_widgets() {
global $post;
    $single_img =  wp_get_attachment_url( get_post_thumbnail_id($post->ID));
?>

    <!--=========== BEGIN COURSE BANNER SECTION ================-->
    <div id="imgBanner" style="background-image: url(<?php echo $single_img;?>)">
      <h2><?php echo get_the_title(); ?></h2>
    </div>
    <!--=========== END COURSE BANNER SECTION ================-->
   
<?php
}


add_action( 'genesis_loop', 'course_custom_loop',1 );

function course_custom_loop() {
    
?>



    <!--=========== BEGIN COURSE BANNER SECTION ================-->
   
        
    <!-- start course content -->
    <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="courseArchive_content">
            <div class="row">
                <!-- start single course -->
<?php
    global $post;
    $args = array(
        
        
        //'post_status' => 'publish',
        //'showposts' => -1,
        'posts_per_page' => 6,
        'post_type' => 'courses',
        'paged' => get_query_var( 'paged' ),
        'order' => 'ASC',
        

        
    );
    //query_posts( $args );
    global $wp_query;
    $wp_query = new WP_Query( $args );

    if( $wp_query->have_posts() ): 
        while( $wp_query->have_posts() ): $wp_query->the_post();
        global $post;
?>
                <div class="col-lg-6 col-md-6 col-sm-6 single_course_outer">
                    <div class="single_course wow fadeInUp">
                        <div class="singCourse_imgarea">
                            <?php echo get_the_post_thumbnail(); ?>
                            <div class="mask">                         
                            <a href="<?php echo get_the_permalink(); ?>" class="course_more">View Course</a>
                            </div>
                        </div>
                        <div class="singCourse_content">
                            <h3 class="singCourse_title">
                                <a href="course-single.html">
                                    <?php echo get_the_title(); ?>
                                </a>
                            </h3>
                            <p class="singCourse_price"><span>$20</span> Per One Month</p>
                            <p><?php echo get_the_content(); ?></p>
                        </div>
                        <div class="singCourse_author">
                            <?php $lecturer_image = get_post_meta($post->ID, 'wpcf-lecturer-image', true); ?>
                            <?php $lecturer = get_post_meta($post->ID, 'wpcf-lecturer', true); ?>
                            <img src="<?php echo $lecturer_image; ?>" alt="img">
                            <p><?php echo $lecturer; ?></p>
                        </div>
                    </div>
                </div>
                 <?php
    endwhile;
    ?>
    <!-- start previous & next button -->
            <div class="single_blog_prevnext">
                <!-- <a href="#" class="prev_post wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;"><i class="fa fa-angle-left"></i>Previous</a>
                <a href="#" class="next_post wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">Next<i class="fa fa-angle-right"></i></a> -->
                <?php genesis_posts_nav(); ?>
            </div>
    <?php
    
    endif;

    wp_reset_query();
?>
                <!-- End single course -->
            </div>
 
            
          
        </div>
    </div>
    <!-- End course content -->

    <!--=========== BEGIN COURSE BANNER SECTION ================-->
 
    <!-- start course archive sidebar -->
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="courseArchive_sidebar">
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Events <span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="http://localhost/wpf-degree/wp-content/uploads/2015/12/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">27.02.15</span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="http://localhost/wpf-degree/wp-content/uploads/2015/12/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">28.02.15</span>                
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="http://localhost/wpf-degree/wp-content/uploads/2015/12/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">28.02.15</span>                
                      </div>
                    </div>
                  </li>                  
                </ul>
              </div>
              <!-- End single sidebar -->
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Quick Links <span class="fa fa-angle-double-right"></span></h2>
                <ul>
                  <li><a href="#">Link 1</a></li>
                  <li><a href="#">Link 2</a></li>
                  <li><a href="#">Link 3</a></li>
                  <li><a href="#">Link 4</a></li>
                  <li><a href="#">Link 5</a></li>
                </ul>
              </div>
              <!-- End single sidebar -->
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Sponsor Add <span class="fa fa-angle-double-right"></span></h2>
                <a class="side_add" href="#"><img src="http://localhost/wpf-degree/wp-content/uploads/2015/12/side-add.jpg" alt="img"></a>
              </div>
              <!-- End single sidebar -->
            </div>
          </div>
          <!-- start course archive sidebar -->


    

    <?php  

}



genesis();
