<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );
include_once( get_stylesheet_directory() . '/new-functions.php' );
//* Set Localization (do not remove)
load_child_theme_textdomain( 'outreach', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'outreach' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', __( 'Outreach Pro Theme', 'outreach' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/outreach/' );
define( 'CHILD_THEME_VERSION', '3.0.1' );

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Enqueue Google fonts
// add_action( 'wp_enqueue_scripts', 'outreach_google_fonts' );
// function outreach_google_fonts() {

// 	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:400,700', array(), CHILD_THEME_VERSION );
	
// }

//* Enqueue Responsive Menu Script
add_action( 'wp_enqueue_scripts', 'outreach_enqueue_responsive_script' );
function outreach_enqueue_responsive_script() {

	wp_enqueue_script( 'outreach-responsive-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0' );

}

//* Add new image sizes
add_image_size( 'home-top', 1140, 460, TRUE );
add_image_size( 'home-bottom', 285, 160, TRUE );
add_image_size( 'sidebar', 300, 150, TRUE );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'height'          => 100,
	'width'           => 340,
) );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for additional color style options
add_theme_support( 'genesis-style-selector', array(
	'outreach-pro-blue' 	=>	__( 'Outreach Pro Blue', 'outreach' ),
	'outreach-pro-orange' 	=> 	__( 'Outreach Pro Orange', 'outreach' ),
	'outreach-pro-purple' 	=> 	__( 'Outreach Pro Purple', 'outreach' ),
	'outreach-pro-red' 		=> 	__( 'Outreach Pro Red', 'outreach' ),
) );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'nav',
	'subnav',
	'site-inner',
	'footer-widgets',
	'footer',
) );

//* Add support for 4-column footer widgets
add_theme_support( 'genesis-footer-widgets', 4 );

//* Set Genesis Responsive Slider defaults
add_filter( 'genesis_responsive_slider_settings_defaults', 'outreach_responsive_slider_defaults' );
function outreach_responsive_slider_defaults( $defaults ) {

	$args = array(
		'location_horizontal'             => 'Left',
		'location_vertical'               => 'bottom',
		'posts_num'                       => '4',
		'slideshow_excerpt_content_limit' => '100',
		'slideshow_excerpt_content'       => 'full',
		'slideshow_excerpt_width'         => '35',
		'slideshow_height'                => '460',
		'slideshow_more_text'             => __( 'Continue Reading', 'outreach' ),
		'slideshow_title_show'            => 1,
		'slideshow_width'                 => '1140',
	);

	$args = wp_parse_args( $args, $defaults );
	
	return $args;
}

//* Hook after post widget after the entry content
add_action( 'genesis_after_entry', 'outreach_after_entry', 5 );
function outreach_after_entry() {

	if ( is_singular( 'post' ) )
		genesis_widget_area( 'after-entry', array(
			'before' => '<div class="after-entry widget-area">',
			'after'  => '</div>',
		) );

}

//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'outreach_author_box_gravatar_size' );
function outreach_author_box_gravatar_size( $size ) {

    return '80';
    
}

//* Remove comment form allowed tags
add_filter( 'comment_form_defaults', 'outreach_remove_comment_form_allowed_tags' );
function outreach_remove_comment_form_allowed_tags( $defaults ) {
	
	$defaults['comment_notes_after'] = '';
	return $defaults;

}

//* Add the sub footer section
add_action( 'genesis_before_footer', 'outreach_sub_footer', 5 );
function outreach_sub_footer() {

	if ( is_active_sidebar( 'sub-footer-left' ) || is_active_sidebar( 'sub-footer-right' ) ) {
		echo '<div class="sub-footer"><div class="wrap">';
		
		   genesis_widget_area( 'sub-footer-left', array(
		       'before' => '<div class="sub-footer-left">',
		       'after'  => '</div>',
		   ) );
	
		   genesis_widget_area( 'sub-footer-right', array(
		       'before' => '<div class="sub-footer-right">',
		       'after'  => '</div>',
		   ) );
	
		echo '</div><!-- end .wrap --></div><!-- end .sub-footer -->';	
	}
	
}

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'outreach' ),
	'description' => __( 'This is the top section of the Home page.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home - Bottom', 'outreach' ),
	'description' => __( 'This is the bottom section of the Home page.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'after-entry',
	'name'        => __( 'After Entry', 'outreach' ),
	'description' => __( 'This is the after entry widget area.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'sub-footer-left',
	'name'        => __( 'Sub Footer - Left', 'outreach' ),
	'description' => __( 'This is the left section of the sub footer.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'sub-footer-right',
	'name'        => __( 'Sub Footer - Right', 'outreach' ),
	'description' => __( 'This is the right section of the sub footer.', 'outreach' ),
) );
//Extra sections
genesis_register_sidebar( array(
	'id'          => 'mid-section-one',
	'name'        => __( 'Mid-Section One', 'outreach' ),
	'description' => __( 'This is the first mid section right below the sliders next section', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'mid-section-two',
	'name'        => __( 'Mid-Section Two', 'outreach' ),
	'description' => __( 'This is the second mid section right below Mid-Section One', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'mid-section-three',
	'name'        => __( 'Mid-Section Three', 'outreach' ),
	'description' => __( 'This is the third mid section right below Mid-Section Two', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'mid-section-four',
	'name'        => __( 'Mid-Section Four', 'outreach' ),
	'description' => __( 'This is the second mid section right below Mid-Section Three', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'mid-section-five',
	'name'        => __( 'Mid-Section Five', 'outreach' ),
	'description' => __( 'This is the second mid section right below Mid-Section Four', 'outreach' ),
) );
//Extra sidebars
genesis_register_sidebar( array(
	'id' => 'course-sidebar',
	'name' => 'Course Sidebar',
	'description' => 'This is the sidebar for Course Page.',
) );

/*-------------------------------------------------Extra Functions------------------------------------------*/

/*Removing Genesis Primary Navigation*/

remove_action( 'genesis_after_header', 'genesis_do_nav' );

/*Custom Header*/

// Filter the title with a custom function
add_filter('genesis_seo_title', 'wap_site_title' );

// Add additional custom style to site header
function wap_site_title( $title ) {

    // Change $custom_title text as you wish
	$custom_title = 'WpF <span>Degree</span>';

	// Don't change the rest of this on down

	// If we're on the front page or home page, use `h1` heading, otherwise us a `p` tag
	$tag = ( is_home() || is_front_page() ) ? 'h1' : 'p';

	// Compose link with title
	$inside = sprintf( '<a href="%s" title="%s">%s</a>', trailingslashit( home_url() ), esc_attr( get_bloginfo( 'name' ) ), $custom_title );

	// Wrap link and title in semantic markup
	$title = sprintf ( '<%s class="site-title" itemprop="headline">%s</%s>', $tag, $inside, $tag );
	return $title;

}

/*Custom Footer*/

remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'sp_custom_footer' );
function sp_custom_footer() {
	?>
	<div class="footer_bottom">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="footer_bootomLeft">
                <p> Copyright &copy; All Rights Reserved</p>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="footer_bootomRight">
                <p>Designed by <a href="http://wpfreeware.com/" rel="nofollow">Wpfreeware.com</a></p>
              </div>
            </div>
          </div>
        </div>
    </div>

	<?php
}


/*To incorporate bootstrap into site*/

add_action('wp_enqueue_scripts', 'wpfdegree_bootstrap_styles');
function wpfdegree_bootstrap_styles() {
    wp_enqueue_style('Bootstrap-style',  get_stylesheet_directory_uri() . '/css/bootstrap.min.css'); 
}

/*To incorporate font-awesome into site*/
add_action('wp_enqueue_scripts', 'wpfdegree_fontawesome_styles');
function wpfdegree_fontawesome_styles() {
    wp_enqueue_style('font-awesome-style',  get_stylesheet_directory_uri() . '/css/font-awesome.min.css'); 
}

/*To incorporate superslides into site*/
add_action('wp_enqueue_scripts', 'wpfdegree_superslide_styles');
function wpfdegree_superslide_styles() {
    wp_enqueue_style('superslides-style',  get_stylesheet_directory_uri() . '/css/superslides.css'); 
}

/*To incorporate slick into site*/
add_action('wp_enqueue_scripts', 'wpfdegree_slick_styles');
function wpfdegree_slick_styles() {
    wp_enqueue_style('slick-style',  get_stylesheet_directory_uri() . '/css/slick.css'); 
}

/*To incorporate circliful into site*/
add_action('wp_enqueue_scripts', 'wpfdegree_circliful_styles');
function wpfdegree_circliful_styles() {
    wp_enqueue_style('circliful-style',  get_stylesheet_directory_uri() . '/css/jquery.circliful.css'); 
}

/*To incorporate tosrus into site*/
add_action('wp_enqueue_scripts', 'wpfdegree_tosrus_styles');
function wpfdegree_tosrus_styles() {
    wp_enqueue_style('tosrus-style',  get_stylesheet_directory_uri() . '/css/jquery.tosrus.all.css'); 
}

/*To incorporate default theme into site*/
add_action('wp_enqueue_scripts', 'wpfdegree_default_theme_styles');
function wpfdegree_default_theme_styles() {
    wp_enqueue_style('default-theme-style',  get_stylesheet_directory_uri() . '/css/themes/default-theme.css'); 
}

/*To incorporate queryloader into site*/
add_action('wp_enqueue_scripts', 'wpfdegree_queryloader_styles');
function wpfdegree_queryloader_styles() {
    wp_enqueue_style('queryloader-style',  get_stylesheet_directory_uri() . '/css/queryLoader.css'); 
}

/*To incorporate animate into site*/
add_action('wp_enqueue_scripts', 'wpfdegree_animate_styles');
function wpfdegree_animate_styles() {
    wp_enqueue_style('animate-style',  get_stylesheet_directory_uri() . '/css/animate.css'); 
}


/*Custom JS script*/
add_action('genesis_before_footer', 'wpfdegree_custom_scripts');
function wpfdegree_custom_scripts() {
    wp_enqueue_script( 'custom-js', get_bloginfo( 'stylesheet_directory' ) . '/js/custom_new.js', array( 'jquery' ), '1.0.0' );
}


/*Custom JS script*/
add_action('genesis_before_footer', 'wpfdegree_queryloader2_scripts');
function wpfdegree_queryloader2_scripts() {
    wp_enqueue_script( 'queryloader2-js', get_bloginfo( 'stylesheet_directory' ) . '/js/queryloader2.min.js', array( 'jquery' ), '1.0.0' );
}

/*Bootstrap JS script*/
add_action('genesis_before_footer', 'wpfdegree_bootstrap_scripts');
function wpfdegree_bootstrap_scripts() {
    wp_enqueue_script( 'bootstrap-js', get_bloginfo( 'stylesheet_directory' ) . '/js/bootstrap.min.js', array( 'jquery' ), '1.0.0' );
}

/*slick JS script*/
add_action('genesis_before_footer', 'wpfdegree_slick_scripts');
function wpfdegree_slick_scripts() {
    wp_enqueue_script( 'slick-js', get_bloginfo( 'stylesheet_directory' ) . '/js/slick.min.js', array( 'jquery' ), '1.0.0' );
}

/*superslides JS script*/
add_action('genesis_before_footer', 'wpfdegree_superslides_scripts');
function wpfdegree_superslides_scripts() {
    wp_enqueue_script( 'superslides-js', get_bloginfo( 'stylesheet_directory' ) . '/js/jquery.superslides.min.js', array( 'jquery' ), '1.0.0' );
}

/*WoWslider JS script*/
add_action('genesis_before_footer', 'wpfdegree_wowslider_scripts');
function wpfdegree_wowslider_scripts() {
    wp_enqueue_script( 'wowslider-js', get_bloginfo( 'stylesheet_directory' ) . '/js/wow.min.js', array( 'jquery' ), '1.0.0' );
}

/*Easing JS script*/
add_action('genesis_before_footer', 'wpfdegree_easing_scripts');
function wpfdegree_easing_scripts() {
    wp_enqueue_script( 'easing-js', get_bloginfo( 'stylesheet_directory' ) . '/js/jquery.easing.1.3.js', array( 'jquery' ), '1.0.0' );
}

/*Animate JS script*/
add_action('genesis_before_footer', 'wpfdegree_animate_scripts');
function wpfdegree_animate_scripts() {
    wp_enqueue_script( 'animate-js', get_bloginfo( 'stylesheet_directory' ) . '/js/jquery.animate-enhanced.min.js', array( 'jquery' ), '1.0.0' );
}

/*circliful JS script*/
add_action('genesis_before_footer', 'wpfdegree_circliful_scripts');
function wpfdegree_circliful_scripts() {
    wp_enqueue_script( 'circliful-js', get_bloginfo( 'stylesheet_directory' ) . '/js/jquery.circliful.min.js', array( 'jquery' ), '1.0.0' );
}

/*Tosrus JS script*/
add_action('genesis_before_footer', 'wpfdegree_tosrus_scripts');
function wpfdegree_tosrus_scripts() {
    wp_enqueue_script( 'tosrus-js', get_bloginfo( 'stylesheet_directory' ) . '/js/jquery.tosrus.min.all.js', array( 'jquery' ), '1.0.0' );
}

/*Old custom JS script*/
add_action('genesis_before_footer', 'wpfdegree_custom_old_scripts');
function wpfdegree_custom_old_scripts() {
    wp_enqueue_script( 'custom-old-js', get_bloginfo( 'stylesheet_directory' ) . '/js/custom.js', array( 'jquery' ), '1.0.0' );
}

add_action('genesis_before_footer', 'wpfdegree_scroll_to_top');
function wpfdegree_scroll_to_top() {
    ?>
    <a href="#" class="scrollToTop" style="transition-property: all; transition-duration: 0.5s; transition-timing-function: cubic-bezier(0.25, 0.1, 0.25, 1); display: block; opacity: 1;">
    	&nbsp;
    </a>
    <?php
}


//Customizing next page and previous page links/-----------------------------------

//* Customize the next page link
add_filter ( 'genesis_next_link_text' , 'sp_next_page_link' );
function sp_next_page_link ( $text ) {
    return 'Next&nbsp;<i class="fa fa-angle-right"></i>';
}

//* Customize the previous page link
add_filter ( 'genesis_prev_link_text' , 'sp_previous_page_link' );
function sp_previous_page_link ( $text ) {
    return '<i class="fa fa-angle-left"></i>&nbsp;Previous';
}


//Customizing next page and previous page links/-----------------------------------

   





/*-------------------------------------------------Extra Functions------------------------------------------*/